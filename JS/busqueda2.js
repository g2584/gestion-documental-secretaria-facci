
function validar() {
  if ($("#searchInput").val().length == 0) {
    alert("Error, no hay texto el cual buscar");
    return false;
  }
}

function validar1() {
  var input = document.getElementById("folderName");
  input.addEventListener("input", function (evt) {
    this.setCustomValidity("");
  });
  input.addEventListener("invalid", function (evt) {
    // Required
    if (this.validity.valueMissing) {
      this.setCustomValidity("Por favor ingrese nombre de carpeta!");
    }
  });
  
  if ($(".inputCreate").val().length > 0) {
    let divs = document.getElementsByClassName("folder").length;
    let btn = document.createElement("div");
    btn.setAttribute("id","folder"+(divs+1));
    btn.setAttribute("class","folder")
    btn.innerHTML =
      $(".inputCreate").val() +
      " <input class='cbx' id='ck"+(divs + 1)+"' type='checkbox'></div>";
    document.getElementById("folders").appendChild(btn);
    $(".newFolder").removeClass("show");
    document.getElementById("folderName").value = "";
  }
}
