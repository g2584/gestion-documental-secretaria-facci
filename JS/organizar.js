
// !!ESTA PENDEJADA ARREGLALA O ELIMINALA XD, bromita, pero ve que haces con esto
// !!Las validaciones del formulario ponlas aca crea un area asi con comentario como hice
// !!para tener un orden del codigo y sepamos diferenciar donde estamos, por eso lo parto
// !!en areas, Pilas oe :v


let inputs = document.querySelectorAll('#form div input');
var adver = document.getElementById('mensaje');
//
    //
        //
            //
                //
                    //
                        //
// *FUNCION PARA SUBIR DOCUMENTO
//Subir los datos del input al formulario
const form = document.getElementById('transactionForm');
form.addEventListener("submit", function(event) {
    event.preventDefault();
    let transactionFormData = new FormData(form);
    let transactionObj = convertFormDataToTransactionObj(transactionFormData);
    let propietario = document.getElementById('transactionOwner');
    let archivo = document.getElementById('transactionFile');
    if (propietario.value == '') {
        adver.innerHTML='Debe ingresar el nombre del propietario.'
    }
    else if (archivo.value == '') {
        adver.innerHTML='Debe seleccionar un archivo para subirlo'
    }
    else{
        adver.innerHTML='';
        saveTransactionObj(transactionObj);
        insertRowTransactionTable(transactionObj);
        form.reset();
    }

})

//Para  generar el array guardado en el localStorage
document.addEventListener("DOMContentLoaded",function(event){
    let transactionObjArray = JSON.parse(localStorage.getItem("transactionData"));
    transactionObjArray.forEach(element => {
        insertRowTransactionTable(element);
    });
});

//por cada ejecucion de la función se retornará un id valido que se asociará
//a cada registro o fila de la tabla de documentos.
function getNewTransactionId(){
    let lastTransactionId = localStorage.getItem("lastTransactionId") || "0";
    let newTransactionId = JSON.parse(lastTransactionId) + 1;
    localStorage.setItem("lastTransactionId", JSON.stringify(newTransactionId))
    return newTransactionId;
}


//Convert To Object
function convertFormDataToTransactionObj(transactionFormData) {
    var date = new Date()
    //Se inserta cada valor en variables para pasarlo a un objeto
    let transactionFileName = transactionFormData.get('transactionFile').name;
    let transactionOwner = transactionFormData.get('transactionOwner');
    let transactionFileDate = date.getFullYear()+"/"+(date.getMonth()+1)+"/"+date.getDate();
    let transactionFileSize = transactionFormData.get('transactionFile').size;
    //Para la eliminación de las filas o registros de el local storage
    //transactionId se encargará de generar el Id para identificar los registros
    let transactionId = getNewTransactionId();
    //Retorna el objerto de los datos guardados
    return{
        "transactionFileName": transactionFileName,
        "transactionOwner": transactionOwner,
        "transactionFileDate": transactionFileDate,
        "transactionFileSize": transactionFileSize,
        //retorna el ID previamente definido
        "transactionId": transactionId
    }
}

//Funcion para agregar datos a la tabla
function insertRowTransactionTable(transactionObj) {
    //una referencia de la tabla, para poder generar la lista
    let transactionTableRef = document.getElementById('transactionTable');
    //Insertando en la ultima fila
    let newTransactionRowRef = transactionTableRef.insertRow(-1);
    newTransactionRowRef.setAttribute("data-transaction-id", transactionObj["transactionId"])
    //insertando en cada celda de la fila, los datos obtenidos
    let newTransactionCellRef = newTransactionRowRef.insertCell(0);
    newTransactionCellRef.textContent = transactionObj["transactionFileName"].split(".")[0];

    newTransactionCellRef = newTransactionRowRef.insertCell(1);
    newTransactionCellRef.textContent = transactionObj["transactionOwner"];
    
    newTransactionCellRef = newTransactionRowRef.insertCell(2);
    newTransactionCellRef.textContent = "."+transactionObj["transactionFileName"].split(".")[1];
    
    newTransactionCellRef = newTransactionRowRef.insertCell(3);
    newTransactionCellRef.setAttribute("class", "numericDate");
    newTransactionCellRef.textContent = transactionObj["transactionFileDate"];
    
    newTransactionCellRef = newTransactionRowRef.insertCell(4);
    newTransactionCellRef.setAttribute("class", "numericSize");
    newTransactionCellRef.textContent = roundToTwo(transactionObj["transactionFileSize"]/1024)+"KB";

    //Se definen variables para la adición de otra columna, esta contendrá
    //un botón para la eliminación de los registros de la tabla
    let newDeleteCell = newTransactionRowRef.insertCell(5);
    //se crea el elemento boton dentro de la celda
    let deleteButton = document.createElement("button");
    //Se define el texto del elemento boton
    deleteButton.textContent="Eliminar";
    newDeleteCell.appendChild(deleteButton);


    //mediante el evento click se define el objetivo del botón, en este caso
    //el objetivo es la etiqueta padre (td)y a su vez el objetivo de esta ultima
    //(tr), usando asi el método Remove() para eliminar toda la fila
    deleteButton.addEventListener("click", (event) => {
        //al hacer clic en eliminar se tiene como objetivo la fila
        let transactionRow = event.target.parentNode.parentNode;
        //de la fila se obtiene el valor del atributo data-transaction-id
        let transactionId = transactionRow.getAttribute("data-transaction-id");
        //elimina la fila del html
        transactionRow.remove();
        //elimina el registro del local storage
        deleteTransactionObj();
        
    })


}
//Funcion de redondeo para el tamaño de los datos.
function roundToTwo(num) {    
    return +(Math.round(num + "e+3")  + "e-3");
}

//se pasa como parametro el transactionId de la transaccion que se quiere eliminar
function deleteTransactionObj(transactionId){
    //se obitnene las transacciones de la base de datos, localstorage en este caso (pasa de json a obj)
    let transactionObjArr = JSON.parse(localStorage.getItem("transactionData"))
    //se busca el indice o posicion de la transaccion a eliminar
    let transactionIndexInArray = transactionObjArr.findIndex(element => element.transactionId === transactionId)
    //se elimina el elemento en esa posicion
    transactionObjArr.splice(transactionIndexInArray, 1)
    //se convierte de obj a json nuevamente
    let transactionArrayJSON = JSON.stringify(transactionObjArr);
    //Guardo mi array de transacción en formato JSON en el localStorage
    localStorage.setItem("transactionData", transactionArrayJSON);
}

//PAra guardar en el localStorage
function saveTransactionObj(transactionObj) {
    //Se llena el array de los objetos que estaba convertido en formato JSON
    let myTransactionArray = JSON.parse(localStorage.getItem("transactionData")) || [];
    //se agrega al array
    myTransactionArray.push(transactionObj);
    //Convierto mi array de transacción a json
    let transactionOjbJSON = JSON.stringify(myTransactionArray);
    //Guardo mi array de transacción en formato JSON en el localStorage
    localStorage.setItem("transactionData", transactionOjbJSON);
}
//
    //
        //
            //
                //
                    //
                        //

// *FUNCIONES PARA ORDENAR LA LISTA DE DOCUMENTOS
//Orden de los string
$(document).ready(() => {
    $('.string').each(function(columna){
        $(this).hover(function(){
            $(this).addClass('resaltar');
        }, function(){
            $(this).removeClass('resaltar');
        });

        $(this).click(function(){
            let registros = $('table').find('tbody > tr').get();

            registros.sort(function(a, b){
                let valor1 = $(a).children('td').eq(columna).text().toLowerCase();
                let valor2 = $(b).children('td').eq(columna).text().toLowerCase();
                
                console.log(valor1, valor2);
                return valor1 < valor2 ? -1 : valor1 > valor2 ? 1 : 0;
            });

            $.each(registros, function(indice, elemento){
                $('tbody').append(elemento);
                console.log(indice, elemento);
            });
        });
    });
    //Orden de las fechas desde la mas reciente hasta la mas antigua
    $('.date').each(function(columna){
        $(this).hover(function(){
            $(this).addClass('resaltar');
        }, function(){
            $(this).removeClass('resaltar');
        });

        $(this).click(function(){
            let registros = $('table').find('tbody > tr').get();

            registros.sort(function(a, b){
                let valor1 = $(a).children('.numericDate').eq(columna).text().toLowerCase();
                let valor2 = $(b).children('.numericDate').eq(columna).text().toLowerCase();

                console.log(valor1, valor2);
                return valor1 > valor2 ? -1 : valor1 < valor2 ? 1 : 0;
            });

            $.each(registros, function(indice, elemento){
                $('tbody').append(elemento);
                console.log(indice, elemento);
            });
        });
    });
    //Orden del tamaño del archivo
    $('.dateE').each(function(columna){
        $(this).hover(function(){
            $(this).addClass('resaltar');
        }, function(){
            $(this).removeClass('resaltar');
        });

        $(this).click(function(){
            let registros = $('table').find('tbody > tr').get();

            registros.sort(function(a, b){
                let valor1 = $(a).children('.numericSize').eq(columna).text().toLowerCase();
                let valor2 = $(b).children('.numericSize').eq(columna).text().toLowerCase();
                let prev = parseInt(valor1);
                let next = parseInt(valor2);
                console.log( prev, next);
                console.log(typeof prev,typeof next);
                return prev > next ? -1 : prev < next ? 1 : 0;
            });

            $.each(registros, function(indice, elemento){
                $('tbody').append(elemento);
                console.log(indice, elemento);
            });
        });
    });
});
